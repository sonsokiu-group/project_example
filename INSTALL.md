1. Cài đặt platform/core , platform/packages/helpers, platform/plugins/core-user
   #Chú ý cần phải được add vào các repo trước khi chạy command
```
    git submodule add git@gitlab.com:workable_packages/platform.git platform/core 
    git submodule add git@gitlab.com:workable_packages/packages/helpers.git platform/packages/helpers
    git submodule add git@gitlab.com:timnha247/tools/core_user.git platform/plugins/core-user
```

2. install composer, install npm
```
    composer install
    
    npm install
```

3. Copy đoạn code sau vào file composer.json thư mục gốc sau đó chạy command
```
"repositories": [
        {
            "type": "path",
            "url": "./platform/core",
            "options": {
                "symlink": true
            }
        },
        {
            "type": "path",
            "url": "./platform/packages/*",
            "options": {
                "symlink": true
            }
        },
        {
            "type": "path",
            "url": "./platform/plugins/*",
            "options": {
                "symlink": true
            }
        }
    ]
```

```
 composer require workable/platform:@dev 
 composer require hz/core-user:@dev
```

4. install webpack
```
    npm i path
    
    npm install sass-loader@^12.1.0 sass --save-dev --legacy-peer-deps
    
    npm install jquery
```

5. Bổ sung đoạn code sau vào file package.json
```
  "scripts": {
        "dev": "npm run development",
        "development": "mix",
        "watch": "mix watch",
        "watch-poll": "mix watch -- --watch-options-poll=1000",
        "hot": "mix watch --hot",
        "prod": "npm run production",
        "production": "mix --production",
        "core-user-dev": "mix --mix-config platform/plugins/core-user/webpack.mix.js",
        "core-user-watch": "mix watch --mix-config platform/plugins/core-user/webpack.mix.js",
        "core-user-prod": "mix --production --mix-config platform/plugins/core-user/webpack.mix.js"
    },
```

6. 
```
#copy 
let path = require("path"); vào file platform/plugins/core-user/webpack.mix.js

#chạy command 
     npm run core-user-dev
# Nếu có lỗi comment  @import '~core_resource_scss/icon/font-awesome/scss/font-awesome' trong
 platform/plugins/core-user/resources/assets/scss/components/sidebar/sidebar.scss và chạy lại
```
